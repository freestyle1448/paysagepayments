package net.astropayments.nbank.payments.controller;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.services.SystemService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequiredArgsConstructor
public class SystemController {
    private final SystemService systemService;

    @PostMapping("/api/account/sub")
    public Callable<Answer> accountSub(Long amount, String currency, String account) {
        return () -> systemService.accountSub(Balance.builder()
                .amount(amount)
                .currency(currency)
                .build(), account).get();

    }

    @PostMapping("/api/account/add")
    public Callable<Answer> accountAdd(Long amount, String currency, String account, MerchantOperDto operDto) {
        if (operDto.getWithdrawalMethod() == null) {
            return () -> systemService.accountAdd(Balance.builder()
                    .amount(amount)
                    .currency(currency)
                    .build(), account).get();
        } else {
            return () -> systemService.accountAddWithMerchOper(Balance.builder()
                    .amount(amount)
                    .currency(currency)
                    .build(), account, operDto).get();
        }
    }
}
