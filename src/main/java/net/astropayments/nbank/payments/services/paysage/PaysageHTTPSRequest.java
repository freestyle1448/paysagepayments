package net.astropayments.nbank.payments.services.paysage;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.PaysageCredentials;
import net.astropayments.nbank.payments.models.RunnerLog;
import net.astropayments.nbank.payments.models.paysage.payouts.PayoutRequest;
import net.astropayments.nbank.payments.models.paysage.payouts.PayoutResponse;
import net.astropayments.nbank.payments.models.paysage.status.StatusResponseHolder;
import net.astropayments.nbank.payments.repositories.RunnerLogsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

import static net.astropayments.nbank.payments.models.RequestTypes.CHECK;
import static net.astropayments.nbank.payments.models.RequestTypes.WITHDRAW;

@Component
@ConfigurationProperties("paysage")
@RequiredArgsConstructor
public class PaysageHTTPSRequest {
    private static final String LOG_MES = "Запрос - {}\nОтвет - {}\n";
    private static final Logger logger = LoggerFactory.getLogger(PaysageHTTPSRequest.class);

    private final RunnerLogsRepository runnerLogsRepository;
    private final RestTemplate restTemplate;
    private String url;

    public CompletableFuture<PayoutResponse> payout(PayoutRequest request) {
        PayoutResponse response;
        String responseString;

        final var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        final var objectMapper = new JsonMapper()
                .enable(DeserializationFeature.UNWRAP_ROOT_VALUE)
                .enable(SerializationFeature.WRAP_ROOT_VALUE);
        String requestAsString;
        try {
            requestAsString = objectMapper.writeValueAsString(request);

            headers.setBasicAuth(request.getPaysageCredentials().getShopId(), request.getPaysageCredentials().getSecret());
            HttpEntity<String> entity = new HttpEntity<>(requestAsString, headers);

            responseString = restTemplate.postForObject(url + "transactions/payouts", entity, String.class);
            response = objectMapper.readValue(responseString, PayoutResponse.class);
        } catch (Exception ex) {
            logger.error("ошибка при запросе к paysage(withdraw)", ex);
            runnerLogsRepository.save(RunnerLog.builder()
                    .transactionNumber(request.getTrackingId())
                    .date(new Date())
                    .requestType(WITHDRAW)
                    .request(request.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        runnerLogsRepository.save(RunnerLog.builder()
                .transactionNumber(request.getTrackingId())
                .date(new Date())
                .requestType(WITHDRAW)
                .request(request.toString())
                .response(responseString)
                .build());
        logger.info(LOG_MES, requestAsString, response);
        return CompletableFuture.completedFuture(response);
    }

    public CompletableFuture<StatusResponseHolder> status(String trackingId, PaysageCredentials credentials) {
        ResponseEntity<StatusResponseHolder> response;

        final var headers = new HttpHeaders();

        try {
            headers.setBasicAuth(credentials.getShopId(), credentials.getSecret());
            HttpEntity<String> entity = new HttpEntity<>(headers);

            response = restTemplate.exchange(url + "v2/transactions/tracking_id/" + trackingId, HttpMethod.GET, entity, StatusResponseHolder.class);
        } catch (Exception ex) {
            logger.error("ошибка при запросе к paysage(status)", ex);
            runnerLogsRepository.save(RunnerLog.builder()
                    .transactionNumber(trackingId)
                    .date(new Date())
                    .requestType(CHECK)
                    .request(null)
                    .fullException(ex.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        runnerLogsRepository.save(RunnerLog.builder()
                .transactionNumber(trackingId)
                .date(new Date())
                .requestType(CHECK)
                .request(null)
                .response(response.toString())
                .build());
        logger.info(LOG_MES, "Статус для " + trackingId, response);
        return CompletableFuture.completedFuture(response.getBody());
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
