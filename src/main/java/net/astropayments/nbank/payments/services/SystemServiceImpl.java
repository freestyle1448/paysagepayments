package net.astropayments.nbank.payments.services;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.models.Account;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.nb.Person;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOper;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperData;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.models.transaction.UserCredentials;
import net.astropayments.nbank.payments.repositories.GatesRepository;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.repositories.TransactionsRepository;
import net.astropayments.nbank.payments.services.nb.NBHTTPSRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static net.astropayments.nbank.payments.models.RequestTypes.IN;
import static net.astropayments.nbank.payments.models.transaction.Status.SUCCESS;

@Service
@Async
@RequiredArgsConstructor
public class SystemServiceImpl implements SystemService {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final GatesRepository gatesRepository;
    private final NBHTTPSRequest nbhttpsRequest;
    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;
    private final TransactionsRepository transactionsRepository;

    @Override
    public CompletableFuture<Answer> accountAdd(Balance balance, String account) {
        logger.info("Обработка операции на ввод");

        Account result;
        try {
            result = retryTemplate.execute((RetryCallback<Account, Throwable>) context -> accountAddTr(balance, account));
        } catch (Throwable ex) {
            logger.error("Пополнение не удалось!", ex);

            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("Пополнение не удалось!" + ex.getLocalizedMessage())
                    .build());
        }

        if (result == null) {
            logger.error("При попытке пополнении аккаунта произошла ошибка!\n");
            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("При попытке пополнении аккаунта произошла ошибка!")
                    .build());
        }

        final var transaction = Transaction.builder()
                .startDate(new Date())
                .endDate(new Date())
                .status(SUCCESS)
                .stage(2)
                .amount(balance)
                .finalAmount(balance)
                .type(IN)
                .receiverCredentials(UserCredentials.builder()
                        .account(account)
                        .build())
                .build();

        transactionsRepository.save(transaction);
        logger.info("Пополнение аккаунта успешно");

        return CompletableFuture.completedFuture(Answer.builder()
                .status("OK")
                .amount(result.getBalance().getAmount())
                .build());
    }

    @Override
    public CompletableFuture<Answer> accountSub(Balance balance, String account) {
        logger.info("Обработка операции на списание");

        Account result;
        try {
            result = retryTemplate.execute((RetryCallback<Account, Throwable>) context -> accountSubTr(balance, account));
        } catch (Throwable ex) {
            logger.error("Списание не удалось!", ex);

            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("Списание не удалось!" + ex.getLocalizedMessage())
                    .build());
        }

        if (result == null) {
            logger.error("При попытке списания с аккаунта произошла ошибка!\n");
            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("При попытке списания с аккаунта произошла ошибка!")
                    .build());
        }

        logger.info("Списание с аккаунта успешно");

        return CompletableFuture.completedFuture(Answer.builder()
                .status("OK")
                .amount(result.getBalance().getAmount())
                .build());
    }

    @Override
    public CompletableFuture<Answer> accountAddWithMerchOper(Balance balance, String account, MerchantOperDto merchantOperDto) {
        final var optionalGate = gatesRepository.findByWithdrawalMethod(merchantOperDto.getWithdrawalMethod());

        if (optionalGate.isEmpty()) {
            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("Гейт не найден!")
                    .build());
        }

        final var gate = optionalGate.get();
        var request = ReqMerchantOper.builder()
                .nbCredentials(gate.getNbCredentials())
                .reqId(UUID.randomUUID().toString())
                .signedDataObject(ReqMerchantOperData.builder()
                        .amount((double) (balance.getAmount() / 100))
                        .currencyCode(balance.getCurrency())
                        .dbCr(merchantOperDto.getDbCr())
                        .maskCardNo(merchantOperDto.getMaskCardNo())
                        .merchantAuthCode(merchantOperDto.getMerchantAuthCode())
                        .payer(Person.builder()
                                .lastName(merchantOperDto.getPayerLastName())
                                .build())
                        .pcOrderNo(merchantOperDto.getPcOrderNo())
                        .rrn(merchantOperDto.getRrn())
                        .receiverInn(merchantOperDto.getReceiverInn())
                        .receiverName(merchantOperDto.getReceiverName())
                        .strAccount(merchantOperDto.getStrAccount())
                        .terminalCode(merchantOperDto.getTerminalCode())
                        .url(merchantOperDto.getUrl())
                        .build())
                .build();

        return nbhttpsRequest.reqMerchantOper(request)
                .thenCompose(reqMerchantOperResponse -> {
                    if (reqMerchantOperResponse.getData().getResponseText() == null) {
                        return CompletableFuture.completedFuture(Answer.builder()
                                .status(reqMerchantOperResponse.getData().getStatus())
                                .err(reqMerchantOperResponse.getData().getResponseText())
                                .build());
                    }

                    return accountAdd(balance, account);
                });
    }

    private Account accountAddTr(Balance balance, String account) {
        var def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        Account receiverAccount;
        try {
            logger.info("Начало пополнения аккаунта");
            receiverAccount = manualRepository.findAndModifyAccountAdd(account
                    , balance);
            if (receiverAccount == null) {
                logger.error("Аккаунт с указанным ID не найден!");

                transactionManager.rollback(status);

                return null;
            }

        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("Ошибка при пополнении аккаунта ", ex);
            throw ex;
        }

        logger.info("Коммитим пополнение");
        transactionManager.commit(status);

        return receiverAccount;
    }

    public Account accountSubTr(Balance balance, String account) {
        var def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        Account senderAccount;
        try {
            logger.info("Начало списания средств с аккаунта");
            senderAccount = manualRepository.findAndModifyAccountSub(account
                    , balance);
            if (senderAccount == null) {
                logger.error("Аккаунт с указанным ID не найден! ");

                transactionManager.rollback(status);

                return null;
            }
            if (senderAccount.getBalance().getAmount() < 0) {
                logger.error("Недостаточно средств на аккаунте отправителя!");

                transactionManager.rollback(status);

                return null;
            }

        } catch (Exception ex) {
            transactionManager.rollback(status);
            //logger.error("Ошибка при списании с аккаунта в выводе", ex);
            throw ex;
        }

        logger.info("Коммитим списание");
        transactionManager.commit(status);
        return senderAccount;
    }
}
