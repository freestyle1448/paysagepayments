package net.astropayments.nbank.payments.services.psb;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.HandlerStatuses;
import net.astropayments.nbank.payments.models.psb.check.PsbCheckResponse;
import net.astropayments.nbank.payments.models.psb.ping.PsbPingResponse;
import net.astropayments.nbank.payments.models.psb.withdraw.PsbWithdrawResponse;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.repositories.SystemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

import static net.astropayments.nbank.payments.models.transaction.Status.IN_PROCESS;
import static net.astropayments.nbank.payments.models.transaction.Status.MANUAL;

@Component
@RequiredArgsConstructor
public class PsbResponseHandler {
    private static final String BAD_MES = "Списание средств неуспешно для транзакции - {}, статус - {}, сообщение - {}";
    private static final String BANK_DECLINED = "Платеж отклонён банком";
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final ManualRepository manualRepository;
    private final SystemRepository systemRepository;

    public void handleWithdraw(PsbWithdrawResponse response, Transaction transaction) {
        final var error = response.getError();

        if (error == null) {
            final var result = response.getResult();
            final var rc = response.getRc();
            final var message = response.getRctext();
            switch (checkPsbStatus(result, rc)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, result, message);
                    systemRepository.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, result);
                    transaction.setStatus(IN_PROCESS);
                    transaction.setStage(1);
                    systemRepository.saveTransaction(transaction);
                    break;
                case ACCEPTED:
                    systemRepository.confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, result);
                    break;
            }
        } else {
            logger.info(BAD_MES
                    , transaction, 0, error);
            systemRepository.declineTransaction(transaction, BANK_DECLINED, error);
        }
    }

    public void handleStatus(PsbCheckResponse response, Transaction transaction) {
        final var error = response.getError();

        if (error == null) {
            final var result = response.getResult();
            final var message = response.getRctext();
            switch (checkPsbStatus(result, message)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, result, message);
                    systemRepository.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case MANUAL:
                    logger.info("Транзакция отменена ПШ - {}, статус - {}, сообщение - {}", transaction, response, message);

                    transaction.setStage(2);
                    transaction.setStatus(MANUAL);
                    transaction.setEndDate(new Date());
                    transaction.setSystemError(message);
                    systemRepository.saveTransaction(transaction);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств ждёт выполнения для транзакции - {}, статус - {}"
                            , transaction, result);
                    break;
                case ACCEPTED:
                    systemRepository.confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, result);
                    break;
            }
        }
    }

    public void handlePing(PsbPingResponse response) {
        if (response.getError() == null) {
            manualRepository.findGateAndSetBalance(response.getGateId(), (long) (Double.parseDouble(response.getAvailableAmount()) * 100));
        }
    }

    private HandlerStatuses checkPsbStatus(String result, String rc) {
        if (result.equals("0") && rc.equals("00")) {
            return HandlerStatuses.ACCEPTED;
        }
        if (result.equals("3")) {
            return HandlerStatuses.MANUAL;
        }

        return HandlerStatuses.DECLINED;
    }
}
