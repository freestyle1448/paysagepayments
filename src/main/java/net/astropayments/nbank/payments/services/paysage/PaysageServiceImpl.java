package net.astropayments.nbank.payments.services.paysage;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.paysage.RecipientBillingAddress;
import net.astropayments.nbank.payments.models.paysage.payouts.PayoutRequest;
import net.astropayments.nbank.payments.models.paysage.payouts.RecipientCreditCard;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.ManualNameRepository;
import net.astropayments.nbank.payments.repositories.SystemRepository;
import net.astropayments.nbank.payments.services.RunnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Random;

import static net.astropayments.nbank.payments.models.transaction.Status.IN_PROCESS;
import static net.astropayments.nbank.payments.models.transaction.Status.WAITING;

@Service
@Async
@RequiredArgsConstructor
public class PaysageServiceImpl implements RunnerService {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final ManualNameRepository manualNameRepository;
    private final RetryTemplate retryTemplate;
    private final SystemRepository systemRepository;
    private final PaysageHTTPSRequest nbHTTPSRequest;
    private final PaysageResponseHandler paysageResponseHandler;

    @Override
    public void withdraw(Transaction transaction, Gate gate) {
        logger.info("Обработка транзакции на вывод");
        if (transaction.getStage() != 3) {
            Object result = null;
            try {
                result = retryTemplate.execute(context -> systemRepository.accountSub(transaction));
            } catch (Exception ex) {
                logger.error("Списание с аккаунта при выводе не удалось!", ex);

                transaction.setStatus(WAITING);
                transaction.setStage(0);

                systemRepository.saveTransaction(transaction);
            }

            if (result == null) {
                logger.error("При попытке списания средств с аккаунта произошла ошибка!\n {}", transaction);
                return;
            }

            logger.info("Списание с аккаунта успешно");
        }

        int year = Calendar.getInstance().get(Calendar.YEAR);
        final var yearMin = year - 1999;
        final var rand = new Random();
        final var yearMax = yearMin + 10;
        final var yearDiff = yearMax - yearMin;
        final var randomYear = rand.nextInt(yearDiff + 1) + yearMin;
        final var month = rand.nextInt(12) + 1;

        PayoutRequest request = PayoutRequest.builder()
                .amount(transaction.getAmount().getAmount())
                .currency(transaction.getAmount().getCurrency())
                .description(transaction.getNote())
                .trackingId(transaction.getTransactionNumber().toString())
                .test(gate.getPaysageCredentials().getIsTest())
                .recipientCreditCard(RecipientCreditCard.builder()
                        .number(transaction.getReceiverCredentials().getCardNumber())
                        .expYear(String.valueOf(randomYear))
                        .expMonth(String.valueOf(month < 10 ? "0" + month : month))
                        .build())
                .recipientBillingAddress(RecipientBillingAddress.builder()
                        .firstName(manualNameRepository.getFirstName())
                        .lastName(manualNameRepository.getLastName())
                        .build())
                .paysageCredentials(gate.getPaysageCredentials())
                .build();

        nbHTTPSRequest.payout(request)
                .whenComplete((response, throwable) -> {
                    if (response != null && throwable == null) {
                        paysageResponseHandler.handleWithdraw(response, transaction);
                    } else {
                        /*declineTransaction(transaction, "Ошибка при  проведении платежа", "Ошибка при получении ответа от PaySage");

                        logger.info("Списание средств неуспешно для транзакции - {}, сообщение - Не удалось получить ответ от PaySage"
                                , transaction);*/

                        transaction.setStage(3);
                        transaction.setStatus(IN_PROCESS);
                        systemRepository.saveTransaction(transaction);

                        logger.info("Списание средств отложено для транзакции - {}, сообщение - Не удалось получить ответ от PaySage"
                                , transaction);
                    }
                });
    }

    @Override
    public void checkTransaction(Transaction transaction, Gate gate) {
        nbHTTPSRequest.status(transaction.getTransactionNumber().toString(), gate.getPaysageCredentials())
                .whenComplete((response, throwable) -> {
                    if (response != null && throwable == null) {
                        paysageResponseHandler.handleStatus(response.getTransactions().get(0), transaction);
                    }
                });
    }

    @Override
    public void ping() {
        //ВОЗМОЖНО ДОБАВЛЮ
    }

    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        systemRepository.declineTransaction(transaction, errorCause, systemError);
    }
}
