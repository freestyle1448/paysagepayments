package net.astropayments.nbank.payments.services.paysage;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.HandlerStatuses;
import net.astropayments.nbank.payments.models.paysage.payouts.PayoutResponse;
import net.astropayments.nbank.payments.models.paysage.status.StatusResponse;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.SystemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static net.astropayments.nbank.payments.models.transaction.Status.IN_PROCESS;

@Component
@RequiredArgsConstructor
public class PaysageResponseHandler {
    private static final String BAD_MES = "Списание средств неуспешно для транзакции - {}, статус - {}, сообщение - {}";
    private static final String BANK_DECLINED = "Платеж отклонён банком";
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final SystemRepository systemRepository;

    public void handleWithdraw(PayoutResponse response, Transaction transaction) {
        final var message = response.getMessage();
        if (response.getStatus() != null) {
            final var status = response.getStatus();

            switch (checkNBStatus(status)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    systemRepository.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case TOCHECK:
                    transaction.setStage(3);
                    transaction.setStatus(IN_PROCESS);
                    systemRepository.saveTransaction(transaction);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    transaction.setStatus(IN_PROCESS);
                    transaction.setStage(1);
                    systemRepository.saveTransaction(transaction);
                    break;
                case ACCEPTED:
                    systemRepository.confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
            }
        } else {
            logger.info(BAD_MES
                    , transaction, 0, message);
            systemRepository.declineTransaction(transaction, BANK_DECLINED, message);
        }
    }

    public void handleStatus(StatusResponse response, Transaction transaction) {
        if (response != null) {
            final var message = response.getMessage();
            final var status = response.getStatus();

            switch (checkNBStatus(status)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    systemRepository.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case TOCHECK:
                    transaction.setStage(3);
                    transaction.setStatus(IN_PROCESS);
                    systemRepository.saveTransaction(transaction);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств ждёт выполнения для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
                case ACCEPTED:
                    systemRepository.confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
            }
        }
    }

    private HandlerStatuses checkNBStatus(String responseText) {
        if (responseText.equals("incomplete")) {
            return HandlerStatuses.CHECK;
        }
        if (responseText.equals("failed") || responseText.equals("expired")) {
            return HandlerStatuses.DECLINED;
        }
        if (responseText.equals("successful")) {
            return HandlerStatuses.ACCEPTED;
        }

        return HandlerStatuses.DECLINED;
    }
}
