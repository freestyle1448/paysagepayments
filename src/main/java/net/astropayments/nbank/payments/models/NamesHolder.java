package net.astropayments.nbank.payments.models;

import javafx.util.Pair;
import lombok.Value;

import java.util.List;
import java.util.Random;

@Value
public class NamesHolder {
    Random rand = new Random();
    List<String> firstNames;
    List<String> lastNames;

    public Pair<String, String> getRandomFirstNameWithSecond() {
        return new Pair<>(firstNames.get(rand.nextInt(firstNames.size())), lastNames.get(rand.nextInt(lastNames.size())));
    }
}
