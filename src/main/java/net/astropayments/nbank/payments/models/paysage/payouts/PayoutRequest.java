package net.astropayments.nbank.payments.models.paysage.payouts;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.PaysageCredentials;
import net.astropayments.nbank.payments.models.paysage.Recipient;
import net.astropayments.nbank.payments.models.paysage.RecipientBillingAddress;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonRootName("request")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PayoutRequest {
    private Long amount;
    private String currency;
    private String description;
    @JsonProperty("tracking_id")
    private String trackingId;
    private Boolean test;
    @Builder.Default
    @JsonProperty("duplicate_check")
    private Boolean duplicateCheck = false;
    @JsonProperty("recipient_credit_card")
    private RecipientCreditCard recipientCreditCard;
    private Recipient recipient;
    @JsonProperty("recipient_billing_address")
    private RecipientBillingAddress recipientBillingAddress;

    @JsonIgnore
    private PaysageCredentials paysageCredentials;
}
