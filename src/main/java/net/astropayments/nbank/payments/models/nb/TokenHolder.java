package net.astropayments.nbank.payments.models.nb;

import net.astropayments.nbank.payments.models.nb.auth.AuthResponse;

import java.util.Date;

public class TokenHolder {
    private volatile AuthResponse authResponse = new AuthResponse(null, null, new Date(), null, null, null);

    public synchronized AuthResponse getAuthResponse() {
        return authResponse;
    }

    public synchronized void setAuthResponse(AuthResponse authResponse) {
        this.authResponse = authResponse;
    }

    public synchronized boolean isExpire() {
        return authResponse.getDateExp().compareTo(new Date()) <= 0;
    }
}
