package net.astropayments.nbank.payments.models.nb.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class File {
    private String privateKeyPath;
    private String publicKeyPath;
}
