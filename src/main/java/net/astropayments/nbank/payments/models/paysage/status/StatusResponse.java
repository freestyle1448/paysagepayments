package net.astropayments.nbank.payments.models.paysage.status;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.paysage.CreditCard;
import net.astropayments.nbank.payments.models.paysage.Recipient;
import net.astropayments.nbank.payments.models.paysage.RecipientBillingAddress;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusResponse {
    private String uid;
    private String status;
    private Long amount;
    private String currency;
    private String description;
    private String type;
    @JsonProperty("payment_method_type")
    private String paymentMethodType;
    @JsonProperty("tracking_id")
    private String trackingId;
    private String message;
    private Boolean test;
    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private Date createdAt;
    @JsonProperty("updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private Date updatedAt;
    @JsonProperty("paid_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private Date paidAt;
    @JsonProperty("expired_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private Date expiredAt;
    @JsonProperty("closed_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private Date closedAt;
    @JsonProperty("settled_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private Date settledAt;
    @JsonProperty("parent_uid")
    private String parentUid;
    private String language;
    private String id;
    private Recipient customer;
    @JsonProperty("credit_card")
    private CreditCard creditCard;
    @JsonProperty("billing_address")
    private RecipientBillingAddress billingAddress;
    private Authorization authorization;
    private Capture capture;
    private String reason;
    private Refund refund;
}
