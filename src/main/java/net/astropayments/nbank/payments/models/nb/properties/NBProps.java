package net.astropayments.nbank.payments.models.nb.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "nb")
public class NBProps {
    private File file;
    private String url;
}
