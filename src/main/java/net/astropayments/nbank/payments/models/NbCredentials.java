package net.astropayments.nbank.payments.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NbCredentials {
    private String customerId;
    private String password;
    private String account;
}
