package net.astropayments.nbank.payments.models.nb.reqmerchantoper;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FiscalData {
    @JsonProperty("TerminalId")
    private String terminalId;
    @JsonProperty("PayNumber")
    private String payNumber;
    @JsonProperty("PayAmount")
    private Double payAmount;
    @JsonProperty("FiscalNumber")
    private String fiscalNumber;
    @JsonProperty("FiscalDoc")
    private String fiscalDoc;
    @JsonProperty("FiscalSign")
    private String fiscalSign;
    @JsonProperty("PayTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss", timezone = "Europe/Moscow")
    private Date payTime;
}
