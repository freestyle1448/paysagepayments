package net.astropayments.nbank.payments.models.psb.ping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class PsbPingResponse {
    @JsonProperty("AVAILABLE_AMOUNT")
    private String availableAmount;
    @JsonProperty("ERROR")
    private String error;

    @JsonIgnore
    private ObjectId gateId;
}
