package net.astropayments.nbank.payments.repositories;

import net.astropayments.nbank.payments.models.LastName;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LastNamesRepository extends MongoRepository<LastName, ObjectId> {
}
