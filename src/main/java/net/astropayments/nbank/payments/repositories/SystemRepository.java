package net.astropayments.nbank.payments.repositories;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Date;

import static net.astropayments.nbank.payments.models.transaction.Status.DENIED;
import static net.astropayments.nbank.payments.models.transaction.Status.SUCCESS;

@Repository
@RequiredArgsConstructor
public class SystemRepository {
    private static final Logger logger = LoggerFactory.getLogger(SystemRepository.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;

    public Object accountSub(Transaction transaction) {
        var def = new DefaultTransactionDefinition();
        def.setName("forTr" + transaction.getTransactionNumber());
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            logger.info("Начало списания средств с аккаунта");
            final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                    , transaction.getFinalAmount());
            if (senderAccount == null) {
                logger.error("Аккаунт с указанным ID не найден! Для транзакции {}", transaction);

                transactionManager.rollback(status);

                transaction.setStatus(DENIED);
                transaction.setStage(2);
                transaction.setSystemError("Аккаунт отправителя не найден!");
                transaction.setErrorReason("Аккаунт отправителя не найден!");
                transaction.setEndDate(new Date());

                saveTransaction(transaction);

                return null;
            }
            if (senderAccount.getBalance().getAmount() < 0) {
                logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

                transactionManager.rollback(status);

                transaction.setStatus(DENIED);
                transaction.setStage(2);
                transaction.setSystemError("INSUFFICIENT_FUNDS");
                transaction.setErrorReason("INSUFFICIENT_FUNDS");
                transaction.setEndDate(new Date());

                saveTransaction(transaction);

                return null;
            }

        } catch (Exception ex) {
            transactionManager.rollback(status);
            //logger.error("Ошибка при списании с аккаунта в выводе", ex);
            throw ex;
        }

        logger.info("Коммитим списание");
        transactionManager.commit(status);
        return new Object();
    }

    public void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            transaction.setStatus(SUCCESS);
            transaction.setStage(2);
            transaction.setEndDate(new Date());

            saveTransaction(transaction);
            logger.info("Транзакция успешно принята(confirmTransaction) - {}"
                    , transaction);
        } else {
            logger.error("Для подтверждения транзакции передали не существующую транзакцию");
        }
    }

    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        if (transaction != null) {
            final var result = retryTemplate.execute(context -> {
                var def = new DefaultTransactionDefinition();
                def.setName("forTr" + transaction.getTransactionNumber());
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

                TransactionStatus status = transactionManager.getTransaction(def);
                try {
                    if ((manualRepository.findAndModifyAccountAdd(transaction.getSenderCredentials().getAccount(), transaction.getFinalAmount()) == null)) {
                        logger.error("Во время отмены транзакции аккаунт не был найден! - {}"
                                , transaction);

                        transactionManager.rollback(status);
                        return null;
                    } else {
                        transactionManager.commit(status);

                        return new Object();
                    }
                } catch (Exception ex) {
                    transactionManager.rollback(status);
                    //logger.error("Ошибка при возврате средств", ex);
                    throw ex;
                }
            });

            if (result != null) {
                transaction.setStatus(DENIED);
                transaction.setStage(1);
                transaction.setErrorReason(errorCause);
                transaction.setSystemError(systemError);
                transaction.setEndDate(new Date());
                saveTransaction(transaction);

                logger.error("Успешный возврат! - {}", transaction);
            }
        } else {
            logger.error("Для отмены транзакции передали не существующую транзакцию");
        }
    }

    public void saveTransaction(Transaction transaction) {
        manualRepository.saveTransaction(transaction);
    }
}
