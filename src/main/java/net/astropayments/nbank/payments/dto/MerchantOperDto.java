package net.astropayments.nbank.payments.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MerchantOperDto {
    private String withdrawalMethod;
    private String date;
    private Integer dbCr;
    private String maskCardNo;
    private String merchantAuthCode;
    private String payerLastName;
    private String pcOrderNo;
    private String rrn;
    private String receiverInn;
    private String receiverName;
    private String strAccount;
    private String terminalCode;
    private String url;
    private String gateName;
}
